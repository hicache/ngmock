#pragma once

#include <algorithm>
#include <cstdint>
#include <gmock/gmock.h>
#include <limits>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)
#include <Windows.h>
#else
#include <sys/mman.h>
#endif

namespace ngmock {

/**
 * @brief 临时性打开和关闭操作系统对于.text段虚拟内存的写保护
 *
 */
class NGMockVMGuard {
public:
    explicit NGMockVMGuard(void* ptr)
        : vm_ptr_(ptr)
    {
#if defined(_WIN32) || defined(_WIN64)
        SYSTEM_INFO info;
        ::GetSystemInfo(&info);
        page_size_ = info.dwPageSize;
#else
        page_size_ = ::getpagesize();
#endif

        vm_page_ = reinterpret_cast<void*>(alignAddress(reinterpret_cast<std::size_t>(vm_ptr_), page_size_));
        // 解除操作系统对虚拟内存的写保护
#if defined(_WIN32) || defined(_WIN64)
        DWORD old_protect;
        ::VirtualProtect(vm_page_, page_size_, PAGE_EXECUTE_READWRITE, &old_protect);
#else
        ::mprotect(vm_page_, page_size_, PROT_READ | PROT_WRITE | PROT_EXEC);
#endif
    }
    ~NGMockVMGuard()
    {
        // 恢复操作系统对虚拟内存的写保护
#if defined(_WIN32) || defined(_WIN64)
        DWORD old_protect;
        ::VirtualProtect(vm_page_, page_size_, PAGE_EXECUTE_READ, &old_protect);
#else
        ::mprotect(vm_page_, page_size_, PROT_READ | PROT_EXEC);
#endif
    }

private:
    inline static std::size_t alignAddress(const std::size_t address, const std::size_t page_size)
    {
        return address & (~(page_size - 1));
    }

private:
    void* vm_ptr_ { nullptr };
    void* vm_page_ { nullptr };
    std::size_t page_size_ { 0 };
};

class NGMockFuncPatch {
public:
    template <typename F1, typename F2>
    static void patch(F1 origin_func, F2 target_func, std::vector<std::uint8_t>& opcodes_bak)
    {
        std::uint8_t* origin_func_ptr = reinterpret_cast<std::uint8_t*>((std::intptr_t&)origin_func);
        std::uint8_t* target_func_ptr = reinterpret_cast<std::uint8_t*>((std::intptr_t&)target_func);
        std::int64_t func_offset = static_cast<std::int64_t>(target_func_ptr - origin_func_ptr);
        std::vector<std::uint8_t> opcodes;

        if (std::abs(func_offset) <= (std::numeric_limits<std::int8_t>::max)()) {
            std::int8_t jmp_offset = static_cast<std::int8_t>(func_offset - 2);
            // jmp jmp_offset
            opcodes.emplace_back(0xeb);
            opcodes.emplace_back(jmp_offset);
        } else if (std::abs(func_offset) <= (std::numeric_limits<std::int32_t>::max)()) {
            std::int32_t jmp_offset = static_cast<std::int32_t>(func_offset - 5);
            // jmp jmp_offset
            opcodes.emplace_back(0xe9);
            std::copy_n(reinterpret_cast<std::uint8_t*>(&jmp_offset), sizeof(std::int32_t), std::back_inserter(opcodes));
        } else {
            // movabs origin_func_ptr, %rax
            opcodes.emplace_back(0x48);
            opcodes.emplace_back(0xb8);
            std::copy_n(reinterpret_cast<std::uint8_t*>(&origin_func_ptr), sizeof(std::intptr_t), std::back_inserter(opcodes));
            // jmp *%rax
            opcodes.emplace_back(0xff);
            opcodes.emplace_back(0xe0);
        }
        opcodes_bak.clear();
        opcodes_bak.reserve(opcodes.size());
        NGMockVMGuard vm_guard(origin_func_ptr);
        std::copy_n(origin_func_ptr, opcodes.size(), std::back_inserter(opcodes_bak));
        std::copy(opcodes.cbegin(), opcodes.cend(), origin_func_ptr);
    }

    template <typename F>
    static void unpatch(F origin_func, const std::vector<std::uint8_t>& opcodes_bak)
    {
        std::uint8_t* origin_func_ptr = reinterpret_cast<std::uint8_t*>((std::intptr_t&)origin_func);
        NGMockVMGuard vm_guard(origin_func_ptr);
        std::copy(opcodes_bak.cbegin(), opcodes_bak.cend(), origin_func_ptr);
    }
};

template <typename T>
class NGSingleton {
public:
    static T& instance()
    {
        static T ins;
        return ins;
    }
};

template <typename T>
class NGMockBase { };

template <typename T>
class NGMock : public NGMockBase<T> { };

template <int uniq>
class NGMockTypeUniq { };

template <typename T>
class NGMockStubEntryPoint { };

template <typename I, typename R, typename... Args>
class NGMockStubEntryPoint<I(R(Args...))> {
public:
    static R entryPoint(Args... args)
    {
        return NGSingleton<NGMock<I(R(Args...))>*>::instance()->stubFunc(std::forward<Args>(args)...);
    }
};

template <typename I, typename C, typename R, typename... Args>
class NGMockStubEntryPoint<I(R (C::*)(Args...))> {
public:
    R entryPoint(Args... args)
    {
        // 插桩后此处的this不再是NGMockStubEntryPoint的对象指针，将表示被插桩成员函数所对应的对象
        return NGSingleton<NGMock<I(R(C::*)(const void*, Args...))>*>::instance()->stubFunc(this, std::forward<Args>(args)...);
    }
};

template <typename I, typename C, typename R, typename... Args>
class NGMockStubEntryPoint<I(R (C::*)(Args...) const)> {
public:
    R entryPoint(Args... args)
    {
        // 插桩后此处的this不再是NGMockStubEntryPoint的对象指针，将表示被插桩成员函数所对应的对象
        return NGSingleton<NGMock<I(R(C::*)(const void*, Args...) const)>*>::instance()->stubFunc(this, std::forward<Args>(args)...);
    }
};

template <typename R, typename... Args>
class NGMockBase<R(Args...)> {
public:
    explicit NGMockBase(const std::string& name)
        : name_(name)
    {
    }

    virtual ~NGMockBase() = default;

    R stubFunc(Args... args)
    {
        mock_.SetOwnerAndName(this, name_.c_str());
        return mock_.Invoke(std::forward<Args>(args)...);
    }

    ::testing::MockSpec<R(Args...)> gmock_stubFunc(::testing::Matcher<Args>... m)
    {
        mock_.RegisterOwner(this);
        return mock_.With(std::move(m)...);
    }

    ::testing::MockSpec<R(Args...)> gmock_stubFunc(const ::testing::internal::WithoutMatchers&, R (*)(Args...))
    {
        return this->gmock_stubFunc(::testing::A<Args>()...);
    }

protected:
    const std::string name_;
    ::testing::FunctionMocker<R(Args...)> mock_;
    std::vector<std::uint8_t> opcodes_bak_;
};

template <typename I, typename R, typename... Args>
class NGMock<I(R(Args...))> : public NGMockBase<R(Args...)> {
public:
    using MockFunctionType = R(Args...);
    NGMock(MockFunctionType func, const std::string& name)
        : NGMockBase<MockFunctionType>(name)
        , origin_func_(func)
    {
        NGSingleton<decltype(this)>::instance() = this;
        NGMockFuncPatch::patch(origin_func_, NGMockStubEntryPoint<I(MockFunctionType)>::entryPoint, NGMockBase<MockFunctionType>::opcodes_bak_);
    }
    ~NGMock() override
    {
        NGMockFuncPatch::unpatch(origin_func_, NGMockBase<MockFunctionType>::opcodes_bak_);
        NGSingleton<decltype(this)>::instance() = nullptr;
    }

private:
    MockFunctionType* origin_func_;
};

template <typename I, typename C, typename R, typename... Args>
class NGMock<I(R (C::*)(const void*, Args...))> : public NGMockBase<R(const void*, Args...)> {
public:
    using StubFunctionType = R(const void*, Args...);
    using MockFunctionType = R (C::*)(Args...);
    NGMock(MockFunctionType func, const std::string& name)
        : NGMockBase<StubFunctionType>(name)
        , origin_func_(func)
    {
        NGSingleton<decltype(this)>::instance() = this;
        NGMockFuncPatch::patch(origin_func_, &NGMockStubEntryPoint<I(MockFunctionType)>::entryPoint, NGMockBase<StubFunctionType>::opcodes_bak_);
    }
    ~NGMock() override
    {
        NGMockFuncPatch::unpatch(origin_func_, NGMockBase<StubFunctionType>::opcodes_bak_);
        NGSingleton<decltype(this)>::instance() = nullptr;
    }

private:
    MockFunctionType origin_func_ { nullptr };
};

template <typename I, typename C, typename R, typename... Args>
class NGMock<I(R (C::*)(const void*, Args...) const)> : public NGMockBase<R(const void*, Args...)> {
public:
    using StubFunctionType = R(const void*, Args...);
    using MockFunctionType = R (C::*)(Args...) const;
    NGMock(MockFunctionType func, const std::string& name)
        : NGMockBase<StubFunctionType>(name)
        , origin_func_(func)
    {
        NGSingleton<decltype(this)>::instance() = this;
        NGMockFuncPatch::patch(origin_func_, &NGMockStubEntryPoint<I(MockFunctionType)>::entryPoint, NGMockBase<StubFunctionType>::opcodes_bak_);
    }
    ~NGMock() override
    {
        NGMockFuncPatch::unpatch(origin_func_, NGMockBase<StubFunctionType>::opcodes_bak_);
        NGSingleton<decltype(this)>::instance() = nullptr;
    }

private:
    MockFunctionType origin_func_ { nullptr };
};

template <typename T>
class NGMockFuncCache {
public:
    using FunctionHashMap = std::unordered_map<std::uintptr_t, std::shared_ptr<T>>;

    static FunctionHashMap& instance()
    {
        return NGSingleton<FunctionHashMap>::instance();
    }
};

class NGMockFactory {
public:
    template <typename I, typename R, typename... Args>
    static const std::shared_ptr<NGMockBase<R(Args...)>> createMock(R function(Args...), const std::string& name)
    {
        using StubFunctionType = R(Args...);
        using MockFunctionType = R(Args...);
        const std::uintptr_t func_addr = static_cast<std::uintptr_t>((std::intptr_t&)function);
        if (NGMockFuncCache<NGMockBase<StubFunctionType>>::instance().contains(func_addr)) {
            return NGMockFuncCache<NGMockBase<StubFunctionType>>::instance()[func_addr];
        }
        NGMockFuncCache<NGMockBase<StubFunctionType>>::instance().insert({ func_addr, std::make_shared<NGMock<I(MockFunctionType)>>(function, name) });
        return createMock<I>(function, name);
    }

    template <typename I, typename C, typename R, typename... Args>
    static const std::shared_ptr<NGMockBase<R(const void*, Args...)>> createMock(R (C::*function)(Args...), const std::string& name)
    {
        using StubFunctionType = R(const void*, Args...);
        using MockFunctionType = R (C::*)(const void*, Args...);
        const std::uintptr_t func_addr = static_cast<std::uintptr_t>((std::intptr_t&)function);
        if (NGMockFuncCache<NGMockBase<StubFunctionType>>::instance().contains(func_addr)) {
            return NGMockFuncCache<NGMockBase<StubFunctionType>>::instance()[func_addr];
        }
        NGMockFuncCache<NGMockBase<StubFunctionType>>::instance().insert({ func_addr, std::make_shared<NGMock<I(MockFunctionType)>>(function, name) });
        return createMock<I>(function, name);
    }

    template <typename I, typename C, typename R, typename... Args>
    static const std::shared_ptr<NGMockBase<R(const void*, Args...)>> createMock(R (C::*function)(Args...) const, const std::string& name)
    {
        using StubFunctionType = R(const void*, Args...);
        using MockFunctionType = R (C::*)(const void*, Args...) const;
        const std::uintptr_t func_addr = static_cast<std::uintptr_t>((std::intptr_t&)function);
        if (NGMockFuncCache<NGMockBase<StubFunctionType>>::instance().contains(func_addr)) {
            return NGMockFuncCache<NGMockBase<StubFunctionType>>::instance()[func_addr];
        }
        NGMockFuncCache<NGMockBase<StubFunctionType>>::instance().insert({ func_addr, std::make_shared<NGMock<I(MockFunctionType)>>(function, name) });
        return createMock<I>(function, name);
    }
};

}

#define NGEXPECT_CALL(func, ...) \
    EXPECT_CALL(*::ngmock::NGMockFactory::createMock<::ngmock::NGMockTypeUniq<__COUNTER__>>(func, #func), stubFunc(__VA_ARGS__))
