




## 1.1 对函数机器码打补丁

### 1.1.1 解除操作系统对.text代码段的写保护

默认情况下不论是Windows还是Linux对于一个程序的.text段都是只有 **读+执行** 的权限，我们要打补丁时就需要临时性的为目标地址增加 **写** 权限。

### 1.1.2 补丁代码

长jmp对应汇编，需要借助寄存器实现，选择rax寄存器是因为在x86架构中默认rax是作为函数返回值的，不会被当作函数入参传递寄存器，这样不会破坏要跳转的目标函数的入参数据
```c++
extern "C" void longJmp();

asm(R"(
    .text
    .global longJmp
longJmp:
    movabs $0x1122334455667788, %rax
    jmp *%rax
)");

```

32位jmp对应汇编，
```c++
extern "C" void longJmp();
asm(R"(
    .text
    .global longJmp
longJmp:
    movabs $0x1122334455667788, %rax
    jmp *%rax
)");

extern "C" void shortJmp();
asm(R"(
    .text
    .global shortJmp
shortJmp:
    .set offset, longJmp - . - 9 # 9=4(.4byte offset)+5(jmp longJmp)
    .4byte offset #使用该语句可在反汇编代码中查看偏移值计算结果
    jmp longJmp
)");
```

超短jmp对应汇编，其中0x11223344为目标函数相对于当前指令的下一条指令的偏移值。offset = longJmp - shortJmp - jmpOpcodeSize
```c++
extern "C" void longJmp();
asm(R"(
    .text
    .global longJmp
longJmp:
    movabs $0x1122334455667788, %rax
    jmp *%rax
)");

extern "C" void shortJmp();
asm(R"(
    .text
    .global shortJmp
shortJmp:
    .set offset, longJmp - . - 3 # 3=1(.byte offset)+2(jmp longJmp)
    .byte offset #使用该语句可在反汇编代码中查看偏移值计算结果
    jmp longJmp
)");

int main()
{
    return 0;
}
```

将上述源码保存至test.cpp，使用`g++ test.cpp && objdump -dz a.exe > test.s`反汇编后的结果：

![](./asserts/images/8bit_jmp.png)

可以看到我们手动计算的结果与汇编器计算的结果一致
