#include "ngmock.h"
#include <gtest/gtest.h>
#include <iostream>

int mock_test1(int val)
{
    return val * val;
}

int test1(int val)
{
    return val;
}

// 测试对全局函数进行插桩打补丁
TEST(Mock, testMockFuncPath)
{
    EXPECT_EQ(2, test1(2));
    std::vector<std::uint8_t> opcodes_bak;
    ngmock::NGMockFuncPatch::patch(test1, mock_test1, opcodes_bak);
    EXPECT_EQ(4, test1(2));
    ngmock::NGMockFuncPatch::unpatch(test1, opcodes_bak);
    EXPECT_EQ(2, test1(2));
}

// 测试mock全局函数，自定义函数返回值
TEST(Mock, testMockGlobalFunc)
{
    // auto mock = std::shared_ptr<ngmock::NGMockBase<int(int)>>(new ngmock::NGMock<ngmock::NGMockTypeUniq<__COUNTER__>>(test1));
    auto mock = ngmock::NGMockFactory::createMock<ngmock::NGMockTypeUniq<__COUNTER__>>(test1, "test1");
    EXPECT_CALL(*mock, stubFunc).WillRepeatedly(::testing::Return(123));
    EXPECT_EQ(test1(1), 123);
}

// 测试mock类非虚函数，自定义函数返回值
TEST(Mock, testMockClassNoStaticNoConstFunc)
{
    class Base {
    public:
        int get()
        {
            return 11;
        }
    };
    Base b;
    auto mock = ngmock::NGMockFactory::createMock<ngmock::NGMockTypeUniq<__COUNTER__>>(&Base::get, "&Base::get");
    EXPECT_CALL(*mock, stubFunc(&b)).WillRepeatedly(::testing::Return(123));
    EXPECT_EQ(b.get(), 123);
}

// 测试使用自定义宏mock类非虚成员函数
TEST(Mock, testMockClassNoStaticFunc)
{
    class Base {
    public:
        int get() const
        {
            return 11;
        }
    };
    Base b;
    NGEXPECT_CALL(&Base::get, &b).WillRepeatedly(::testing::Return(123));
    EXPECT_EQ(b.get(), 123);
}

// 在没有对象指针的情况下对非虚成员函数mock
TEST(Mock, testMockClassFuncNotObjectPointer)
{
    class Base {
    public:
        void set(int val)
        {
            val_ = val;
        }
        int get()
        {
            std::cout << "Base::get" << std::endl;
            return val_;
        }

    private:
        int val_ { 58 };
    };
    // 忽略this指针，强行打桩
    NGEXPECT_CALL(&Base::get, ::testing::_).WillRepeatedly(::testing::Return(24));
    // 此时无论将Base::val修改成多少，都会返回mock预设的返回值
    Base b;
    b.set(100);
    EXPECT_EQ(b.get(), 24);
}

int main(int argc, char* argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}